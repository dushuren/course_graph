#!/bin/bash

declare -A prereqs
declare -A coreqs

# regexs
RE_COURSE_DESC='^[A-Z]{3} [0-9]{3} [A-Z].*$'
RE_PREREQS='Pre(-)?requisites?:'
RE_COREQS='Co(-)?requisites?:'
RE_COURSE='[A-Z]{3} [0-9]{3}'

match() #(line, regex)
{
	echo $1 | grep -Pq "$2"
	return $?
}

extract() #(line, regex)
{
	echo $1 | grep -Po "$2"
}

asdf()
{
	for i in "${prereqs[$1]}"
	do
		if [[ -n $i ]]; then
			a=(); a+=("$i")
			echo "$1:"
			for j in $a; do echo -e "\t$j"; [[ $j == $2 ]] && return 1 || asdf "$j" "$2"; done
		fi
	done
	
	return 0
}

parse_reqs() # (line, regex, prefix)
{
	local raw_req req=()
	OIFS=$IFS
	IFS=$'\n'
	# we use grep here instead of =~ since bash doesn't support lookahead/behinds and greediness
	if match "$1" "$2.*(?=\.)"; then	# reqs that are all on one line
		raw_req=$(extract "$1" "$2.*(?=\.)")
	elif match "$1" "$2.*[^.]$"; then # reqs that run off of the first line
		raw_req=$(extract "$1" "$2.*[^.]$")
		last_req=$3	# so we don't include coreqs as prereqs and vice-versa
	elif match "$1" '^.*?(?=\.)' && [[ $last_req == $3 ]]; then	# continued reqs that end on new line
		raw_req=$(extract "$1" '^.*?(?=\.)')
	elif [[ $last_req == $3 ]]; then # continued reqs that continue onto another line
		raw_req=$1
	fi 

	[[ -n $raw_req ]] && req+=($(extract "$raw_req" "$RE_COURSE"))
	[[ -z $req ]] && return
	if [[ $3 == "Pre" ]]; then
		for r in $req
		do 
			asdf "$curr_course" "$r" && prereqs[$curr_course]=${prereqs[$curr_course]}$(echo -e "\n$r")
		done
		#prereqs[$curr_course]=${prereqs[$curr_course]}$(echo -e "\n$req")
	else
		coreqs[$curr_course]=${coreqs[$curr_course]}$(echo -e "\n$req")
	fi
	IFS=$OIFS
}

if [[ $# != 2 ]]; then
	echo "Usage: $0 <course catalog txt file> <course section>"
	exit
fi

num_lines=$(wc -l $1 | awk {'print $1'})
curr_line=0

while read line
do
	echo -ne "Parsing course catalog ("
	echo -ne "$(echo "scale=1;100*$curr_line/$num_lines" | bc)% complete)\r"
	((curr_line+=1))
	if [[ $line =~ $RE_COURSE_DESC ]]; then
		curr_course="${line:0:7}"
		prereqs[$curr_course]=""
		req_found=0
	else
		if [[ $line =~ $RE_PREREQS ]] \
		|| [[ $line =~ $RE_COREQS ]]  \
		|| [[ $req_found == 1 ]]; then
			parse_reqs "$line" $RE_PREREQS "Pre"
			parse_reqs "$line" $RE_COREQS "Co"
			req_found=1
		fi
	fi
done < $1

echo "Parsing course catalog (100% complete)"
echo "Generating graph file..."

echo "digraph $2 {" > $2_courses.txt
course_section="$2 [0-9]{3}"
for i in "${!prereqs[@]}"
do
	if [[ $i =~ $course_section ]]; then
		if [[ -n "${coreqs[$i]}" ]]; then
			while read line
			do
			 [[ -z $line ]] && continue
			 echo -e "\t{rank=same; \"$line\" -> \"$i\";}" >> $2_courses.txt
			done <<< "${coreqs[$i]}"
		fi
		if [[ -z ${prereqs[$i]} ]]; then
			echo -e "\t\"$i\";" >> $2_courses.txt
		else
			while read line
			do
				[[ -z $line ]] && continue
				echo -e "\t\"$line\" -> \"$i\";" >> $2_courses.txt
			done <<< "${prereqs[$i]}"
		fi
	fi
done
echo "}" >> $2_courses.txt

dot -Tpng -o$2_courses.png $2_courses.txt
echo "Done."
