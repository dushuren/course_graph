This script parses the Quincy College course catalog and generates a graph of a selected major's course sequence. The course catalog pdf must first be converted to a .txt. I used pdftotext 0.24.3. The script uses GraphViz's DOT to make the graph, which outputs a PNG.

Some course's pre/co requisites are written different from all of the rest in the catalog. Majority are in the form "ABC 123" but some I found were written "ABC123" or "ABC123/456". I didn't account for all of them but the script can be updated to handle those.

Usage: make_course_graph <course catalog .txt> <course section XXX>